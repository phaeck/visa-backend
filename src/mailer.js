const mailer = require('nodemailer');

const FIELD_NAME_RU_MAP = {
  name: 'Имя',
  source: 'Источник',
  phone: 'Телефон',
  email: 'Электронная почта'
};

const account = {
  user: 'robot@visav.ru',
  pass: 'nfU46Kt1',
  reciever: 'vladimir@visav.ru'
};
const transporter = mailer.createTransport({
  host: 'smtp.yandex.ru',
  port: 465,
  secure: true,
  auth: {
    user: account.user,
    pass: account.pass
  }
});


const getMailConfig = (data) => {
  const mail = {
    from: `noreply <${account.user}>`,
    to: account.reciever,
    subject: `Заявка (${data.source})`,
    html: `<b>Дата создания:</b> ${new Date()}<br>`
  };
  Object.keys(data).forEach(field => {
    mail.html += `<b>${FIELD_NAME_RU_MAP[field]}:</b> ${data[field]}<br>`;
  });
  return mail;
};

const sendMail = (body) => {
  return transporter.sendMail(getMailConfig(body))
    .then(() => ({success: true}));
}

module.exports = {
  sendMail
};