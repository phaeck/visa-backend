const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const serve = require('koa-static');
const logger = require('koa-logger');
const { historyApiFallback } = require('koa2-connect-history-api-fallback');

const router = require('./router');

const app = new Koa();

app
  .use(historyApiFallback({ whiteList: ['/api'] }))
  .use(logger())
  .use(serve('.'))
  .use(serve('webapp'))
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods())

app.listen(3000);
