const { sendMail } = require('../mailer');
const router = require('koa-router')();

router.post('/api/request', async (ctx, next) => {
  try {
    const formKeys = Object.keys(ctx.request.body);
    if (!formKeys.length || formKeys.some(field => !ctx.request.body[field])) {
      ctx.throw(500, 'Fields are empty');
    }

    ctx.body = await sendMail(ctx.request.body);
    await next();
  } catch (err) {
    ctx.status = err.statusCode || err.status || 500;
    ctx.body = err;
  }
});

module.exports = router;