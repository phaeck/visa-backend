FROM node:13-alpine

# ARG GIT_USER=phaeck
# ARG GIT_PASS=Phaecker1

# prepare backend
COPY package.json package.json
RUN npm install
COPY . .
EXPOSE 3000

# fetch and build frontend
RUN apk add --update git && \
  rm -rf /tmp/* /var/cache/apk/*
RUN git clone https://phaeck:Phaecker1@bitbucket.org/phaeck/site-visa.git ./site
RUN npm install @vue/cli-service --prefix site
RUN npm run build --prefix site
RUN mkdir webapp
RUN mv ./site/dist/* webapp
RUN rm -rf ./site

CMD [ "npm", "run", "start:prod" ]